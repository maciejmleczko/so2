#include <thread>
#include <atomic>
#include <string>
#include <iostream>
#include <chrono>
#include <vector>

#include "Kolonista.h"
#include "Robot.h"
#include "Writer.hpp"

int main(int argc, char* argv[]) {
	std::atomic<bool> end = false;
	std::atomic<int> end_kol = 5;
	std::atomic<int> end_rob = 3;
	SektorMieszkalny sektorMieszkalny;
	SektorBadan sektorBadan;
	SektorHydroponiki sektorHydroponiki(end_rob);
	SektorUtylizcji sektorUtylizcji(end_rob);
	SektorMagazynowy sektorMagazynowy;
	Lazik lazik;
	std::vector<Kolonista> kolonisci;
	for(size_t i = 0; i < 5; i++) {
		kolonisci.push_back(Kolonista(i, sektorMieszkalny, sektorBadan, sektorMagazynowy, lazik, end, end_kol));
	}
	std::vector<Robot> roboty;
	for(size_t i = 0; i < 3; i++) {
		roboty.push_back(Robot(sektorMieszkalny, sektorBadan, sektorHydroponiki, sektorUtylizcji, sektorMagazynowy, end_kol, end_rob));
	}

	auto ui_thread_fun = [&]() {
		Writer ui{kolonisci, roboty, sektorMieszkalny, sektorBadan, sektorHydroponiki, sektorUtylizcji, sektorMagazynowy};
		ui.start(end);
	};

	std::thread ui_thread(ui_thread_fun);
	ui_thread.detach();

	std::thread sektorHydroponikiThrd(std::ref(sektorHydroponiki));
	std::thread sektorUtylizcjiThrd(std::ref(sektorUtylizcji));


	std::vector<std::thread> watki_robotow;
	for(size_t i = 0; i < 3; i++) {
		watki_robotow.push_back(std::thread(std::ref(roboty[i])));
	}

	std::vector<std::thread> watki_kolonistow;
	for(size_t i = 0; i < 5; i++) {
		watki_kolonistow.push_back(std::thread(std::ref(kolonisci[i])));
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}

	for(size_t i = 0; i < 5; i++) {
		watki_kolonistow[i].join();
	}

	for(size_t i = 0; i < 3; i++) {
		watki_robotow[i].join();
	}
	sektorHydroponikiThrd.join();
	sektorUtylizcjiThrd.join();


}
