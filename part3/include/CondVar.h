#ifndef CONDVAR_H
#define CONDVAR_H

#include <mutex>
#include <condition_variable>
#include <chrono>

class CondVar {
public:
    CondVar () {}
    
    void notify() {
        std::unique_lock<std::mutex> lock(mtx);
        cv.notify_one();
    }
    void wait() {
        std::unique_lock<std::mutex> lock(mtx);
        cv.wait(lock);
    }

private:
    std::mutex mtx;
    std::condition_variable cv;
};
#endif