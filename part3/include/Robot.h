#ifndef ROBOT_H
#define ROBOT_H

#include "SektorMieszkalny.h"
#include "SektorBadan.h"
#include "SektorHydroponiki.h"
#include "SektorMagazynowy.h"
#include "SektorUtylizacji.h"

enum struct RobotStan {
	Czeka,
	Transportuje_probke,
	Transportuje_owoc,
	Transportuje_odpad,
	Transportuje_nawoz,
	Laduje_baterie
};
class Robot {
public:

	std::atomic<uint8_t> electric_charge = 100;
	std::atomic<int>& end_kol;
	std::atomic<int>& end_rob;

	Robot(SektorMieszkalny& sektorMieszkalny, SektorBadan& sektorBadan, 
		SektorHydroponiki& sektorHydroponiki,SektorUtylizcji& sektorUtylizcji, SektorMagazynowy& sektorMagazynowy, std::atomic<int>& end_kol, std::atomic<int>& end_rob) 
	: sektorMieszkalny(sektorMieszkalny)
	, sektorBadan(sektorBadan) 
	, sektorHydroponiki(sektorHydroponiki)
	, sektorUtylizcji(sektorUtylizcji) 
	, sektorMagazynowy(sektorMagazynowy) 
	, end_kol(end_kol)
	, end_rob(end_rob)
	{}

	Robot(const Robot& robot) 
	: sektorMieszkalny(robot.sektorMieszkalny)
	, sektorBadan(robot.sektorBadan) 
	, sektorHydroponiki(robot.sektorHydroponiki)
	, sektorUtylizcji(robot.sektorUtylizcji)
	, sektorMagazynowy(robot.sektorMagazynowy) 
	, electric_charge(robot.electric_charge.load()) 
	, end_kol(robot.end_kol)
	, end_rob(robot.end_rob)
	{}

	void operator() () {
		while(end_kol) {
			routine();
		}
		end_rob--;
	}
	std::string getState() {
		if(state == RobotStan::Czeka) {
			return "Czeka";
		}
		else if(state == RobotStan::Transportuje_probke) {
			return "Transportuje probke";
		}
		else if(state == RobotStan::Transportuje_owoc) {
			return "Transportuje owoc";
		}
		else if(state == RobotStan::Transportuje_odpad) {
			return "Transportuje odpad";
		}
		else if(state == RobotStan::Transportuje_nawoz) {
			return "Transportuje nawoz";
		}
		else if(state == RobotStan::Laduje_baterie) {
			return "Laduje baterie";
		}		
		else return "Unknown state";
	}

private:
	SektorMieszkalny& sektorMieszkalny;
	SektorBadan& sektorBadan;
	SektorHydroponiki& sektorHydroponiki;
	SektorUtylizcji& sektorUtylizcji;
	SektorMagazynowy& sektorMagazynowy;
	RobotStan state = RobotStan::Czeka;

	void routine() {
		{
			std::unique_lock<std::mutex> lock(sektorMieszkalny.owoce_mtx);
			if(sektorMieszkalny.owoce.empty()) {
				state = RobotStan::Transportuje_owoc;
				lock.unlock();
				fetchowoctask();
				std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::hour));
				electric_charge-=random_num(1,10);
			}
		}
		{
			std::unique_lock<std::mutex> lock(sektorMieszkalny.odpady_mtx);
			if(!sektorMieszkalny.odpady.empty()) {
				state = RobotStan::Transportuje_odpad;
				sektorMieszkalny.odpady.pop_front();
				lock.unlock();
				std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::hour));
				cleanrubbishtask();
				electric_charge-=random_num(1,10);
			}
		}
		{
			std::unique_lock<std::mutex> lock(sektorBadan.requestListProbki_mtx);
			if(sektorBadan.probkiqueue.empty()) {
				state = RobotStan::Transportuje_probke;
				lock.unlock();
				fetchprobkatask();
				std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::hour));
				electric_charge-=random_num(1,10);
			}
		}
		{
			std::unique_lock<std::mutex> lock(sektorHydroponiki.owoce_mtx);
			if(!sektorHydroponiki.owoce.empty()) {
				state = RobotStan::Transportuje_owoc;
				lock.unlock();
				transportowoctask();
				std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::hour));
				electric_charge-=random_num(1,10);
			}
		}
		state = RobotStan::Czeka;
		std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::hour));
		checkbattery();
	}

	void fetchprobkatask() { //
		{
			std::lock_guard<std::mutex> probka_mtx(sektorMagazynowy.probki_mtx);
			sektorMagazynowy.probki.pop_back();
		}
		
		{
			std::lock_guard<std::mutex> probka_mtx(sektorBadan.probkiqueue_mtx);
			sektorBadan.probkiqueue.emplace_back();
		}
		std::unique_lock<std::mutex> lock(sektorBadan.probka_waitqueue_mtx);
		sektorBadan.cvar_probka_waitqueue.notify_one();
	}

	void fetchowoctask() { //
		{
			std::lock_guard<std::mutex> lock(sektorMagazynowy.owoce_mtx);
			sektorMagazynowy.owoce.pop_front();
		}
		{
			std::lock_guard<std::mutex> lock(sektorMieszkalny.owoce_mtx);
			sektorMieszkalny.owoce.emplace_back();
		}
		{
			std::unique_lock<std::mutex> lock(sektorMieszkalny.owoc_waitqueue_mtx);
			sektorMieszkalny.cvar_owoc_waitqueue.notify_one();
		}
	}

	void cleanrubbishtask() { //
		{
			std::lock_guard<std::mutex> odpady_lock(sektorUtylizcji.odpady_mtx);
			sektorUtylizcji.odpady.push_back(Odpad());
		}
		{
			std::unique_lock<std::mutex> lock(sektorUtylizcji.odp_mtx);
			sektorUtylizcji.cv_odp.notify_all();
		}
		{
			std::unique_lock<std::mutex> lock(sektorUtylizcji.fert_mtx);
			sektorUtylizcji.cv_fert.wait(lock, [&]() {
				std::lock_guard<std::mutex> lock_nawz(sektorUtylizcji.nawoz_mtx);
				return !sektorUtylizcji.nawoz.empty();
			});
			sektorUtylizcji.nawoz.pop_front();
		}
		transportfertilizertask();
	}

	void transportfertilizertask() {
		state = RobotStan::Transportuje_nawoz;
		std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::hour));
		sektorHydroponiki.num_of_fertilizers++;
	}

	void transportowoctask() {
		{
			std::lock_guard<std::mutex> lock(sektorHydroponiki.owoce_mtx);
			sektorHydroponiki.owoce.pop_front();
		}
		{
			std::lock_guard<std::mutex> lock(sektorMagazynowy.owoce_mtx);
			sektorMagazynowy.owoce.emplace_back();
		}
	}

	void chargebattery() {
		state = RobotStan::Laduje_baterie;
		while(electric_charge!=100) {
			std::this_thread::sleep_for(std::chrono::milliseconds(30 * TimeOnMars::minute));
			electric_charge+=10;
			if(electric_charge>100) {
				electric_charge=100;
			}
		}
	}

	void checkbattery() {
		if(electric_charge<=20) {
			chargebattery();
		}
	}

	uint64_t random_num(uint64_t begin, uint64_t end) {
		static thread_local std::random_device rd;
		static thread_local std::mt19937 gen(rd());
		std::uniform_int_distribution<uint64_t> dist(begin,end);
		return dist(gen);
	}
};
#endif
