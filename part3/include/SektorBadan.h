#ifndef SEKTORBADAN_H
#define SEKTORBADAN_H
#include <list>
#include <mutex>

#include "Probka.h"
class SektorBadan {
public:
    std::list<Probka> probkiqueue;
    std::mutex probkiqueue_mtx;
    std::list<int> requestListProbki;
    std::mutex requestListProbki_mtx;
    std::atomic<uint8_t> progres = 0;
    std::mutex probka_waitqueue_mtx;
	std::condition_variable cvar_probka_waitqueue;
};
#endif
