#ifndef WRITER_H
#define WRITER_H
#include <iostream>
#include <string>
#include <atomic>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <thread>
#include <mutex>
#include <ncurses.h>
#include <math.h>
#include "Kolonista.h"
#include "Robot.h"

class Writer {
	std::vector<Kolonista>& kolonisci;
	std::vector<Robot>& roboty;
	SektorMieszkalny& sektorMieszkalny;
	SektorBadan& sektorBadan;
	SektorHydroponiki& sektorHydroponiki;
	SektorUtylizcji& sektorUtylizcji;
	SektorMagazynowy& sektorMagazynowy;
	unsigned int lines, columns;
    //WINDOW *window = nullptr;
	WINDOW *window_kol = nullptr;
	WINDOW *window_rob = nullptr;
	WINDOW *window_miesz = nullptr;
	WINDOW *window_mag = nullptr;
	WINDOW *window_hydro = nullptr;
	WINDOW *window_badan = nullptr;
	WINDOW *window_utyl = nullptr;
	WINDOW *window_lazik = nullptr;
	
public:
	Writer(std::vector<Kolonista>& kolonisci, std::vector<Robot>& roboty, SektorMieszkalny& sektorMieszkalny, SektorBadan& sektorBadan,
		SektorHydroponiki& sektorHydroponiki,SektorUtylizcji& sektorUtylizcji,SektorMagazynowy& sektorMagazynowy) 
		:kolonisci(kolonisci)
		, roboty(roboty)
		, sektorMieszkalny(sektorMieszkalny)
		, sektorBadan(sektorBadan)
		, sektorHydroponiki(sektorHydroponiki)
		, sektorUtylizcji(sektorUtylizcji)
		, sektorMagazynowy(sektorMagazynowy) {
		setlocale(LC_ALL,"");
		initscr();
	    getmaxyx(stdscr, lines, columns);
		nodelay(stdscr, TRUE);
	    keypad(stdscr, true);
    	cbreak();
		curs_set(0);
   		noecho();
	    start_color();
    	//use_default_colors();
		
		init_pair(1, COLOR_WHITE, COLOR_BLACK);
    	init_pair(2, COLOR_BLACK, COLOR_GREEN);
		init_pair(3, COLOR_BLACK, COLOR_YELLOW);
		init_pair(4, COLOR_BLACK, COLOR_RED);

		init_pair(5, COLOR_BLACK, COLOR_RED);
		init_pair(6, COLOR_BLACK, COLOR_GREEN);
		init_pair(7, COLOR_BLACK, COLOR_WHITE);

		init_pair(8, COLOR_BLACK, COLOR_YELLOW);

		init_pair(9, COLOR_WHITE, COLOR_CYAN);
		init_pair(10, COLOR_WHITE, COLOR_BLUE);

		//window = newwin(lines - 10, columns - 15, 1, 4);
		window_kol = newwin(9, 36, 1, 4);
		window_rob = newwin(7, 36, 10, 4);
		window_miesz = newwin(16, 36, 1, 46);
		window_hydro = newwin(9, 36, 1, 88);
		window_mag = newwin(7, 36, 10, 88);
		window_badan = newwin(9, 36, 1, 130);
		window_utyl = newwin(7, 36, 10, 130);
		window_lazik = newwin(15, 162, 18, 4);
    	//keypad(window, true);

    	box(window_kol, 0, 0);
		box(window_rob, 0, 0);
		box(window_miesz, 0, 0);
		box(window_hydro, 0, 0);
		box(window_mag, 0, 0);
		box(window_utyl, 0, 0);
		box(window_badan, 0, 0);
		box(window_lazik, 0, 0);
		wattron(window_kol,A_BOLD);
		mvwprintw(window_kol, 1, 13, "%s", "Koloniści");
    	mvwprintw(window_kol, 2, 1, "%s", "Id");
    	mvwprintw(window_kol, 2, 5, "%s", "Stan");
		mvwprintw(window_kol, 2, 26, "%s", "Głód");
		wattroff(window_kol,A_BOLD);
		wattron(window_rob,A_BOLD);
		mvwprintw(window_rob, 1, 14, "%s", "Roboty");
    	mvwprintw(window_rob, 2, 1, "%s", "Id");
    	mvwprintw(window_rob, 2, 5, "%s", "Stan");
		mvwprintw(window_rob, 2, 26, "%s", "Bateria");
		wattroff(window_rob,A_BOLD);
		wattron(window_miesz,A_BOLD);
		mvwprintw(window_miesz, 1, 10, "%s", "Sektor Mieszkalny");
		mvwprintw(window_miesz, 3, 1, "%s", "Sluza");
		mvwprintw(window_miesz, 5, 1, "%s", "Toaleta");
		mvwprintw(window_miesz, 7, 1, "%s", "Kuchnia");
		mvwprintw(window_miesz, 9, 1, "%s", "Łózka");
		mvwprintw(window_miesz, 11, 1, "%s", "Śmietnik");
		mvwprintw(window_miesz, 13, 1, "%s", "Ładowarki");
		wattron(window_miesz,A_BOLD);
		wattron(window_hydro,A_BOLD);
		mvwprintw(window_hydro, 1, 9, "%s", "Sektor Hydroponiki");
		mvwprintw(window_hydro, 3, 1, "%s", "Owoce");
		mvwprintw(window_hydro, 5, 1, "%s", "Nawóz");
		mvwprintw(window_hydro, 7, 1, "%s", "Wzrost");
		wattroff(window_hydro,A_BOLD);
		wattron(window_mag,A_BOLD);
		mvwprintw(window_mag, 1, 9, "%s", "Sektor Magazynowy");
		mvwprintw(window_mag, 3, 1, "%s", "Owoce");
		mvwprintw(window_mag, 5, 1, "%s", "Próbki");
		wattroff(window_mag,A_BOLD);
		wattron(window_badan,A_BOLD);
		mvwprintw(window_badan, 1, 12, "%s", "Sektor Badań");
		mvwprintw(window_badan, 3, 1, "%s", "Próbki");
		mvwprintw(window_badan, 5, 1, "%s", "Progres");
		mvwprintw(window_badan, 7, 1, "%s", "Stanowiska");
		wattroff(window_badan,A_BOLD);
		wattron(window_utyl,A_BOLD);
		mvwprintw(window_utyl, 1, 9, "%s", "Sektor Utylizacji");
		wattroff(window_utyl,A_BOLD);
		//mvwprintw(window_lazik, 6, 1, "%s",  "                                    _____________                                                                                                              ");
		//mvwprintw(window_lazik, 7, 1, "%s",  "                                   /             \\                                                                                                             ");
		//mvwprintw(window_lazik, 8, 1, "%s",  "                                  /               \\                                                                                                            ");
		//mvwprintw(window_lazik, 9, 1, "%s",  "                                 /                 \\                                                                                                           ");
		//mvwprintw(window_lazik, 10, 1, "%s", "   ____________________         /                   |                                                                                                           ");
		//mvwprintw(window_lazik, 11, 1, "%s", "  /                    \\=======/                     \\=    ,-.      ,-.                                                                                        /");
		//mvwprintw(window_lazik, 12, 1, "%s", " /                      \\=====/                       \\== ( o )----( o )                                                                                   /#");
		//mvwprintw(window_lazik, 13, 1, "%s", "                                                           `-'      `-'                                                                                    /##");
   		refresh();

    	wrefresh(window_kol);
		wrefresh(window_rob);
		wrefresh(window_miesz);
		wrefresh(window_hydro);
		wrefresh(window_mag);
		wrefresh(window_badan);
		wrefresh(window_utyl);
		wrefresh(window_lazik);
    	refresh();
	}
	~Writer() {
		endwin();
	}
	void start(std::atomic<bool>& end) {
		int op;
		unsigned int strona = 1;
		unsigned int pos_per_page;
		unsigned int page_limit = 1;

		if(kolonisci.size() <= lines - 14) {
			pos_per_page = kolonisci.size();
		}
		else {
			pos_per_page = lines - 14;
			page_limit = kolonisci.size()/(lines - 14) + 1;
		}

		while((op = getch()) and !end) {
			for(size_t j = 0; j < 5; j++) {
    			//mvwprintw(window_kol, 3 + j, 1, "%s", "  ");
    			mvwprintw(window_kol, 3 + j, 5, "%s", "                      ");
				//mvwprintw(window_kol, 11 + j, 1, "%s", "  ");
    			//mvwprintw(window_rob, 3 + j, 1, "%s", "  ");
    			
				//mvwprintw(window_rob, 11 + j, 1, "%s", "  ");
			}




		mvwprintw(window_lazik, 6, 1, "%s",  "                                              _____________                                                                                               ");
		mvwprintw(window_lazik, 7, 1, "%s",  "                                             /             \\                                                ______                                       ");
		mvwprintw(window_lazik, 8, 1, "%s",  "                                            /               \\                                              /******\\                                      ");
		mvwprintw(window_lazik, 9, 1, "%s",  "                                           /     ________    \\                                            /********\\                                   ");
		mvwprintw(window_lazik, 10, 1, "%s", "\\            ____________________         /     |        |    \\                                          /**********\\              _____  _____           ______");
		mvwprintw(window_lazik, 11, 1, "%s", "*\\          /       _____        \\=======/      ----------     \\                                        /************\\            /*****\\/*****\\         /******");
		mvwprintw(window_lazik, 12, 1, "%s", "**\\        /       |     |        \\=====/                  _____\\                                      /**************\\          /**************\\       /*******");
		mvwprintw(window_lazik, 13, 1, "%s", "***\\      /         -----          \\   /                  |      \\==                                  /****************\\        /****************\\     /********");

		static bool lazik_flag;
		for(size_t i = 0; i < 5; i++) {
			if(kolonisci[i].getState() == "Jedzie lazikiem") {
				lazik_flag = true;
			}
			if(kolonisci[i].getState() == "Bada") {
				mvwprintw(window_lazik, 12, 21+i, "%s", "#");
			}
			else if(kolonisci[i].getState() == "Czeka na kombinezon" or kolonisci[i].getState() == "Ubiera kombinezon" or kolonisci[i].getState() == "Wychodzi na zew" or kolonisci[i].getState() == "Zdejmuję kombinezon") {
				mvwprintw(window_lazik, 13, 60+i, "%s", "#");
			}
			else if(kolonisci[i].getState() == "Na powierzchni") {
				mvwprintw(window_lazik, 13, 5+i, "%s", "#");
			}
			else if(kolonisci[i].getState() == "Śpi" or kolonisci[i].getState() == "Toaleta" or kolonisci[i].getState() == "Posilek") {
				mvwprintw(window_lazik, 10, 50+i, "%s", "#");
			}
		}
			if(lazik_flag)  {
				static uint8_t jazda = 0;
				static int kierunek = 1;
				mvwprintw(window_lazik, 11, 70+jazda, "%s", " ,-.  #  ,-.");
				mvwprintw(window_lazik, 12, 70+jazda, "%s", "( o )-|-( o )");
				mvwprintw(window_lazik, 13, 70+jazda, "%s", " `-'     `-'");
				jazda+=kierunek;
				if(jazda==20) {
					kierunek=-1;
				}
				if(jazda==0) {
					kierunek=1;
					lazik_flag = false;
				}	
			}
			else {
				mvwprintw(window_lazik, 11, 70, "%s", " ,-.     ,-.");
				mvwprintw(window_lazik, 12, 70, "%s", "( o )-|-( o )");
				mvwprintw(window_lazik, 13, 70, "%s", " `-'     `-'");
			}

			for(size_t j = 0; j < 3; j++) {
				mvwprintw(window_rob, 3 + j, 5, "%s", "                      ");
			}
			mvwprintw(window_miesz, 11, 10, "%s", "                      ");

			for(size_t i = 0; i < 5; i++) {
    			mvwprintw(window_kol, 3 + i, 1, "%u", i);
    			mvwprintw(window_kol, 3 + i, 5, "%s", kolonisci[i].getState().c_str());
				mvwprintw(window_kol, 3 + i, 26, "%s", "     ");
				if(kolonisci[i].hunger.load()>=60) {
					wattron(window_kol,COLOR_PAIR(2));
					mvwprintw(window_kol, 3 + i, 26, "%s", " | | ");
				}
				else if(kolonisci[i].hunger.load()<60 and kolonisci[i].hunger.load()>20) {
					wattron(window_kol,COLOR_PAIR(3));
					mvwprintw(window_kol, 3 + i, 26, "%s", " | ");
				}
				else {
					wattron(window_kol,COLOR_PAIR(4));
					mvwprintw(window_kol, 3 + i, 26, "%s", " ");
				}
				wattron(window_kol,COLOR_PAIR(1));
			}
			for(size_t i = 0; i < 3; i++) {
    			mvwprintw(window_rob, 3 + i, 1, "%u", i);
    			mvwprintw(window_rob, 3 + i, 5, "%s", roboty[i].getState().c_str());
				mvwprintw(window_rob, 3 + i, 26, "%s", "     ");
				if(roboty[i].electric_charge.load()>=60) {
					wattron(window_rob,COLOR_PAIR(2));
					mvwprintw(window_rob, 3 + i, 26, "%s", " | | ");
				}
				else if(roboty[i].electric_charge.load()<60 and roboty[i].electric_charge.load()>20) {
					wattron(window_rob,COLOR_PAIR(3));
					mvwprintw(window_rob, 3 + i, 26, "%s", " | ");
				}
				else {
					wattron(window_rob,COLOR_PAIR(4));
					mvwprintw(window_rob, 3 + i, 26, "%s", " ");
				}
				wattron(window_rob,COLOR_PAIR(1));
			}
			//sektor mieszkalny
			//sluza
			if(sektorMieszkalny.busy_wew) {
				wattron(window_miesz,COLOR_PAIR(5));
				mvwprintw(window_miesz, 3, 14, "%s", "X|");
			}
			else {
				wattron(window_miesz,COLOR_PAIR(6));
				mvwprintw(window_miesz, 3, 14, "%s", "O|");
			}
			if(sektorMieszkalny.busy_zew) {
				wattron(window_miesz,COLOR_PAIR(5));
				mvwprintw(window_miesz, 3, 16, "%s", "X");
			}
			else {
				wattron(window_miesz,COLOR_PAIR(6));
				mvwprintw(window_miesz, 3, 16, "%s", "O");
			}
			//toaleta
			if(sektorMieszkalny.busy_toilet) {
				wattron(window_miesz,COLOR_PAIR(5));
				mvwprintw(window_miesz, 5, 15, "%s", " ");
			}
			else {
				wattron(window_miesz,COLOR_PAIR(6));
				mvwprintw(window_miesz, 5, 15, "%s", " ");
			}
			//kuchnia
			if(sektorMieszkalny.busy_kitchen) {
				wattron(window_miesz,COLOR_PAIR(5));
				mvwprintw(window_miesz, 7, 15, "%s", " ");
			}
			else {
				wattron(window_miesz,COLOR_PAIR(6));
				mvwprintw(window_miesz, 7, 15, "%s", " ");
			}
			//lozka

			for(size_t i = 0; i < 5; i++) {
				if(kolonisci[i].getState() == "Śpi") {
					wattron(window_miesz,COLOR_PAIR(5));
					mvwprintw(window_miesz, 9, 14 + i + i, "%s", " ");
				}
				else {
					wattron(window_miesz,COLOR_PAIR(6));
					mvwprintw(window_miesz, 9, 14 + i + i, "%s", " ");
				}
			}
			//smieci
			size_t num_of_rubbish;
			{
				std::lock_guard<std::mutex> odpady_lock(sektorMieszkalny.busy_beds_mtx);
				num_of_rubbish = sektorMieszkalny.odpady.size();
			}
			wattron(window_miesz,COLOR_PAIR(8));
			for(size_t i = 0; i < num_of_rubbish; i++) {
				mvwprintw(window_miesz, 11, 14 + i + i, "%s", " ");
			}
			//ladowarki
			size_t num_of_charging = 0;
			{

			}
			if(num_of_charging==2) {
				wattron(window_miesz,COLOR_PAIR(5));
				mvwprintw(window_miesz, 13, 14, "%s", " ");
				mvwprintw(window_miesz, 13, 16, "%s", " ");
			}
			else if(num_of_charging==1) {
				wattron(window_miesz,COLOR_PAIR(5));
				mvwprintw(window_miesz, 13, 14, "%s", " ");
				wattron(window_miesz,COLOR_PAIR(2));
				mvwprintw(window_miesz, 13, 16, "%s", " ");
			}
			else {
				wattron(window_miesz,COLOR_PAIR(2));
				mvwprintw(window_miesz, 13, 14, "%s", " ");
				mvwprintw(window_miesz, 13, 16, "%s", " ");
			}
			wattron(window_miesz,COLOR_PAIR(1));
			//hydro
			size_t num_of_owoc_hydro;
			{
				std::lock_guard<std::mutex> owoc_lock(sektorHydroponiki.owoce_mtx);
				num_of_rubbish = sektorHydroponiki.owoce.size();
			}
			wattron(window_hydro,COLOR_PAIR(6));
			for(size_t i = 0; i < num_of_rubbish; i++) {
				mvwprintw(window_hydro, 3, 14 + i + i, "%s", " ");
			}
			wattron(window_hydro,COLOR_PAIR(1));
			mvwprintw(window_hydro, 5, 14, "%d", sektorHydroponiki.num_of_fertilizers.load());
			wattron(window_hydro,COLOR_PAIR(1));
			mvwprintw(window_hydro, 7, 14 , "%s", "|");
			if(sektorHydroponiki.growth_status.load() == 100) {
				wattron(window_hydro,COLOR_PAIR(6));
				mvwprintw(window_hydro, 7, 15 , "%s", "    ");
				wattron(window_hydro,COLOR_PAIR(1));
				mvwprintw(window_hydro, 7, 19 , "%s", "|");
			}
			else if(sektorHydroponiki.growth_status.load() == 75) {
				wattron(window_hydro,COLOR_PAIR(6));
				mvwprintw(window_hydro, 7, 15 , "%s", "   ");
				wattron(window_hydro,COLOR_PAIR(1));
				mvwprintw(window_hydro, 7, 18 , "%s", " ");
				mvwprintw(window_hydro, 7, 19 , "%s", "|");
			}
			else if(sektorHydroponiki.growth_status.load() == 50) {
				wattron(window_hydro,COLOR_PAIR(6));
				mvwprintw(window_hydro, 7, 15 , "%s", "  ");
				wattron(window_hydro,COLOR_PAIR(1));
				mvwprintw(window_hydro, 7, 17 , "%s", "  ");
				mvwprintw(window_hydro, 7, 19 , "%s", "|");
			}
			else if(sektorHydroponiki.growth_status.load() == 25) {
				wattron(window_hydro,COLOR_PAIR(6));
				mvwprintw(window_hydro, 7, 15 , "%s", " ");
				wattron(window_hydro,COLOR_PAIR(1));
				mvwprintw(window_hydro, 7, 16 , "%s", "   ");
				mvwprintw(window_hydro, 7, 19 , "%s", "|");
			}
			else {
				wattron(window_hydro,COLOR_PAIR(1));
				mvwprintw(window_hydro, 7, 15 , "%s", "    ");
				mvwprintw(window_hydro, 7, 19 , "%s", "|");
			}
			//magazyn
			size_t owoc_size_mag, probki_size_mag;
			{
				std::lock_guard<std::mutex> owoc_lock(sektorMagazynowy.owoce_mtx);
				owoc_size_mag = sektorMagazynowy.owoce.size();
			}
			{
				std::lock_guard<std::mutex> owoc_lock(sektorMagazynowy.probki_mtx);
				probki_size_mag = sektorMagazynowy.probki.size();
			}
			mvwprintw(window_mag, 3, 14, "%d", owoc_size_mag);
			mvwprintw(window_mag, 5, 14, "%d", probki_size_mag);
			wattron(window_mag,COLOR_PAIR(1));


			wattron(window_utyl,COLOR_PAIR(1));
			//badan
			size_t probki_size_bad;
			{
				std::lock_guard<std::mutex> probki_lock(sektorBadan.probkiqueue_mtx);
				probki_size_bad = sektorBadan.probkiqueue.size();
			}
			mvwprintw(window_badan, 3, 14, "%s", "              ");
			wattron(window_badan,COLOR_PAIR(9));
			for(size_t i = 0; i < probki_size_bad; i++) {
				mvwprintw(window_badan, 3, 14 + i + i, "%s", " ");
			}
			size_t progress_bar = sektorBadan.progres/10;
			wattron(window_badan,COLOR_PAIR(1));
			mvwprintw(window_badan, 5, 13, "%s", "|");
			for(size_t i = 0; i < 10; i++) {
				mvwprintw(window_badan, 5, 14 + i, "%s", " ");
			}
			wattron(window_badan,COLOR_PAIR(10));
			for(size_t i = 0; i < progress_bar; i++) {
				mvwprintw(window_badan, 5, 14 + i, "%s", " ");
			}
			wattron(window_badan,COLOR_PAIR(1));
			mvwprintw(window_badan, 5, 23, "%s", "|");
			for(size_t i = 0; i < 5; i++) {
				if(kolonisci[i].getState() == "Bada") {
					wattron(window_badan,COLOR_PAIR(5));
					mvwprintw(window_badan, 7, 14 + i + i, "%s", " ");
				}
				else {
					wattron(window_badan,COLOR_PAIR(6));
					mvwprintw(window_badan, 7, 14 + i + i, "%s", " ");
				}
			}
			wattron(window_badan,COLOR_PAIR(1));
			//utylizacja
			static bool panim = true;
				mvwprintw(window_utyl, 3, 1, "%s", "                                  ");
				mvwprintw(window_utyl, 4, 1, "%s", "               ###                ");
				mvwprintw(window_utyl, 5, 1, "%s", "                                  ");
			if(panim) {
				mvwprintw(window_utyl, 5, 1, "%s", "- - - - - - - -###- - - - - - - -");
				panim = false;
			}
			else {
				mvwprintw(window_utyl, 5, 1, "%s", " - - - - - - - ### - - - - - - - ");
				panim = true;
			}
			static bool animnotfinished1 = false;
			static bool animnotfinished2 = false;
			static bool animnotfinished3 = false;
			if(roboty[0].getState() == "Transportuje odpad" or animnotfinished1) {
				animnotfinished1 = true;
				static char znak1 = '~';
				static int z1 = 0;
				mvwprintw(window_utyl, 4, 1+z1++, "%c", znak1);
				if(z1 == 17) {
					znak1 = '*';
				}
				if(z1 == 34) {
					z1 = 0;
					znak1 = '~';
					animnotfinished1 = false;
				}
			}
			if(roboty[1].getState() == "Transportuje odpad" or animnotfinished2) {
				animnotfinished2 = true;
				static char znak2 = '~';
				static int z2 = 0;
				mvwprintw(window_utyl, 4, 1+z2++, "%c", znak2);
				if(z2 == 17) {
					znak2 = '*';
				}
				if(z2 == 34) {
					z2 = 0;
					znak2 = '~';
					animnotfinished2 = false;
				}
			}
			if(roboty[2].getState() == "Transportuje odpad" or animnotfinished3) {
				animnotfinished3 = true;
				static char znak3 = '~';
				static int z3 = 0;
				mvwprintw(window_utyl, 4, 1+z3++, "%c", znak3);
				if(z3 == 17) {
					znak3 = '*';
				}
				if(z3 == 34) {
					z3 = 0;
					znak3 = '~';
					animnotfinished3 = false;
				}
			}
			

    		wrefresh(window_kol);
			wrefresh(window_rob);
			wrefresh(window_miesz);
			wrefresh(window_hydro);
			wrefresh(window_mag);
			wrefresh(window_utyl);
			wrefresh(window_badan);
			wrefresh(window_lazik);
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			if(op == 113) {
				end = true;
			}
		
		}
	}
};

#endif
