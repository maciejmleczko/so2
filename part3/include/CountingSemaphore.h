#ifndef COUNTINGSEMAPHORE_H
#define COUNTINGSEMAPHORE_H

#include <mutex>
#include <condition_variable>
#include <chrono>

class CoutingSemaphore {
public:
    CoutingSemaphore (int count= 0) : count(count) {}
    
    void notify() {
        std::unique_lock<std::mutex> lock(mtx);
        count++;
        cv.notify_one();
    }
    void wait() {
        std::unique_lock<std::mutex> lock(mtx);
        while(count == 0) {
            cv.wait(lock);
        }
        count--;
    }

private:
    std::mutex mtx;
    std::condition_variable cv;
    int count;
};
#endif