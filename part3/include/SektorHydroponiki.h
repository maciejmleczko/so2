#ifndef SEKTORHYDROPONIKI_H
#define SEKTORHYDROPONIKI_H
#include <list>
#include <mutex>
#include <atomic>

#include "Owoc.h"

class SektorHydroponiki {
public:
    std::atomic<uint64_t> num_of_fertilizers = 0;
    std::atomic<uint8_t> growth_status = 0;
    std::list<Owoc> owoce;
    std::mutex owoce_mtx;
    std::atomic<int>& end_rob;

    SektorHydroponiki(std::atomic<int>& end_rob) :end_rob(end_rob) {

    }


    void operator() () {
        while(end_rob) {
            if(num_of_fertilizers) {
                {
                    growth_status = 0;
                    std::lock_guard<std::mutex> odp_lock(owoce_mtx);
                    owoce.push_back(Owoc());
                    num_of_fertilizers--;
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::hour));
                for(int i = 0; i < 4; i ++) {
                    growth_status+=25;
                    std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::hour));
                    
                }
            }
            else {
                std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::hour));
            }
        }
    }

};
#endif
