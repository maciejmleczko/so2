#ifndef SEKTORMAGAZYNOWY_H
#define SEKTORMAGAZYNOWY_H

#include <list>
#include <mutex>

#include "Probka.h"
#include "Owoc.h"

class SektorMagazynowy {
public:
    std::list<Owoc> owoce;
    std::mutex owoce_mtx;
    std::list<Probka> probki;
    std::mutex probki_mtx;
    SektorMagazynowy() {
        for(size_t i = 0; i< 5; i++) {
             owoce.emplace_back();
        }
        for(size_t i = 0; i < 10; i++) {
            probki.emplace_back();
        }
       
    }
};
#endif
