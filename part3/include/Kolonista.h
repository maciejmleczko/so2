#ifndef KOLONISTA_H
#define KOLONISTA_H
#include <chrono>
#include <atomic>
#include <thread>
#include <string>
#include <random>
#include <condition_variable>

#include "SektorMieszkalny.h"
#include "SektorBadan.h"
#include "SektorMagazynowy.h"
#include "Lazik.h"
#include "Lozko.h"
#include "Kombinezon.h"
#include "TimeOnMars.h"
#include "CountingSemaphore.h"
#include "CondVar.h"

enum struct KolonistaStan {
	Spi,
	Kuchnia,
	Toaleta,
	Bada,
	Poszukuje_probek,
	Przerwa,
	Ubiera_kombinezon,
	Czeka_na_kombinezon,
	Zdejmuje_kombinezon,
	Jedzie_lazikiem,
	Wychodzi_na_zew
};

class Kolonista {
public:
	KolonistaStan state = KolonistaStan::Spi;
	int id;
	std::atomic<uint8_t> hunger = 100; // {100,99,...,0} 0 => smierc
	std::atomic<bool>& end;
	std::atomic<int>& end_kol;


	Kolonista(int id, SektorMieszkalny& sektorMieszkalny, SektorBadan& sektorBadan, SektorMagazynowy& sektorMagazynowy, Lazik& lazik, std::atomic<bool>& end, std::atomic<int>& end_kol) 
	: id(id)
	, sektorMieszkalny(sektorMieszkalny)
	, sektorBadan(sektorBadan)
	, sektorMagazynowy(sektorMagazynowy)
	, lazik(lazik)
	, end(end) 
	, end_kol(end_kol)
	{}

	Kolonista(const Kolonista& _kolonista) 
	: state(_kolonista.state)
	, hunger(_kolonista.hunger.load())
	, sektorMieszkalny(_kolonista.sektorMieszkalny)
	, sektorBadan(_kolonista.sektorBadan)
	, sektorMagazynowy(_kolonista.sektorMagazynowy)
	, lazik(_kolonista.lazik)
	, end(_kolonista.end) 
	, end_kol(_kolonista.end_kol)
	{}

	void sen() {
		state = KolonistaStan::Spi;
		std::lock_guard<std::mutex> lck(sektorMieszkalny.lozka[id].mtx);
		{
			std::lock_guard<std::mutex> busy_bed_lock(sektorMieszkalny.busy_beds_mtx);
			sektorMieszkalny.busy_beds[id] = true;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds((random_num(0, 5) + 3) * TimeOnMars::hour));
		{
			std::lock_guard<std::mutex> busy_bed_lock(sektorMieszkalny.busy_beds_mtx);
			sektorMieszkalny.busy_beds[id] = true;
		}
	}
	void badania() {
		state = KolonistaStan::Bada;
		
		{
			std::unique_lock<std::mutex> lock(sektorBadan.probka_waitqueue_mtx);
			sektorBadan.cvar_probka_waitqueue.wait(lock, [&]() { 
				std::lock_guard<std::mutex> queuelock(sektorBadan.probkiqueue_mtx);
				return !sektorBadan.probkiqueue.empty(); });
		}
		{
			std::lock_guard<std::mutex> probka_mtx(sektorBadan.probkiqueue_mtx);
			sektorBadan.probkiqueue.pop_front();
		}
		
		std::this_thread::sleep_for(std::chrono::milliseconds((random_num(0, 3) + 2) * TimeOnMars::hour));
		sektorBadan.progres+=10;
		if(sektorBadan.progres==100) {
			sektorBadan.progres = 0;
		}

	}
	void posilek() {
		state = KolonistaStan::Kuchnia;
		sektorMieszkalny.busy_kitchen = true;

		{
			std::unique_lock<std::mutex> lock(sektorMieszkalny.owoc_waitqueue_mtx);
			sektorMieszkalny.cvar_owoc_waitqueue.wait(lock, [&]() { 
				std::lock_guard<std::mutex> queuelock(sektorMieszkalny.owoce_mtx);
				return !sektorMieszkalny.owoce.empty(); });
		}
		{
			std::lock_guard<std::mutex> probka_mtx(sektorMieszkalny.owoce_mtx);
			sektorMieszkalny.owoce.pop_front();
		}


		bool gotowe_jedzenie = false;
		uint8_t status_przygotowania = 0;
		static std::atomic<bool> starving = false;
		if(hunger <= 20) {
			starving = true;
		}
		//przygotuj posilek
		while(gotowe_jedzenie) {
			sektorMieszkalny.kuchnia.mtx.lock();
			while(status_przygotowania != 100) {
				std::this_thread::sleep_for(std::chrono::milliseconds(10 * TimeOnMars::minute));
				status_przygotowania += 20;
				//check if sb is starving
				if(starving and hunger > 20) {
				 	sektorMieszkalny.kuchnia.mtx.unlock();
					std::this_thread::sleep_for(std::chrono::milliseconds(10 * TimeOnMars::minute));
					break;
				}
			}
			if(status_przygotowania==100) {
				gotowe_jedzenie = true;
				if(starving and hunger <= 20) {
					starving = false;
				} 
			}
		}
		sektorMieszkalny.busy_kitchen = false;
		//zjedz go
		std::this_thread::sleep_for(std::chrono::milliseconds((random_num(0, 1) + 1) * TimeOnMars::hour));
		hunger = 100;
		//wyrzuc smieci
		std::lock_guard<std::mutex> odp_lock(sektorMieszkalny.odpady_mtx);
		sektorMieszkalny.odpady.emplace_back();	
	}
	void toaleta() {
		state = KolonistaStan::Toaleta;
		std::lock_guard<std::mutex> lck(sektorMieszkalny.toaleta.mtx);
		sektorMieszkalny.busy_toilet = true;
		std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::hour));
		//std::lock_guard<std::mutex> odp_lock(sektorMieszkalny.odpady_mtx);
		//sektorMieszkalny.odpady.emplace_back();
		sektorMieszkalny.busy_toilet = false;	
		std::lock_guard<std::mutex> odp_lock(sektorMieszkalny.odpady_mtx);
		sektorMieszkalny.odpady.emplace_back();
	}
	void poszukiwanieprobek() {
		//załoz kombinezon
		state = KolonistaStan::Czeka_na_kombinezon;
		static CoutingSemaphore csem_kombinezony(3);
		csem_kombinezony.wait();
		state = KolonistaStan::Ubiera_kombinezon;
		std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::hour));

		state = KolonistaStan::Wychodzi_na_zew;
		bool went_through = false;
		//przejdz przez sluze
		while(!went_through) {
			std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::minute));
			sektorMieszkalny.busy_wew = true;
			sektorMieszkalny.sluza.wew_sluza.lock();
			std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::minute));
			if(!sektorMieszkalny.sluza.zew_sluza.try_lock()) {
				sektorMieszkalny.sluza.wew_sluza.unlock();
			}
			else {
				sektorMieszkalny.busy_wew = false;
				sektorMieszkalny.busy_zew = true;
				sektorMieszkalny.sluza.wew_sluza.unlock();
				went_through = true;
				std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::minute));
				sektorMieszkalny.sluza.zew_sluza.unlock();
				sektorMieszkalny.busy_zew = false;
			}
		}

		state = KolonistaStan::Poszukuje_probek;
		static std::atomic<bool> aval = true;
		bool already_waited = false;
		bool done = false;

		while(!done) {
			if(aval) {
				//jedz lazikiem
				aval = false;
				std::lock_guard<std::mutex> lck(lazik.mtx);
				state = KolonistaStan::Jedzie_lazikiem;
				std::this_thread::sleep_for(std::chrono::milliseconds(8 * TimeOnMars::hour));
				done = true;
				aval = true;
			}
			else {
				if(!already_waited) {
					already_waited = true;
					std::this_thread::sleep_for(std::chrono::milliseconds(5*TimeOnMars::minute));
				}
				else {
					done = true;
					//albo na nogach
					std::this_thread::sleep_for(std::chrono::milliseconds((random_num(0, 5) + 2) * TimeOnMars::hour));
				}
			}
		}
 
		//przejdz przez sluze
		sektorMieszkalny.busy_zew = true;
		sektorMieszkalny.sluza.zew_sluza.lock();
		std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::minute));
		sektorMieszkalny.sluza.wew_sluza.lock();
		sektorMieszkalny.busy_wew = true;
		sektorMieszkalny.busy_zew = false;
		sektorMieszkalny.sluza.zew_sluza.unlock();
		std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::minute));
		sektorMieszkalny.sluza.wew_sluza.unlock();
		sektorMieszkalny.busy_wew = false;

		//zdejmij kombinezon
		state = KolonistaStan::Zdejmuje_kombinezon;
		std::this_thread::sleep_for(std::chrono::milliseconds(1 * TimeOnMars::hour));
		csem_kombinezony.notify();
		std::lock_guard<std::mutex> probka_mtx(sektorMagazynowy.probki_mtx);
		sektorMagazynowy.probki.emplace_back();
	}
	void operator() () {
		while(!end) {
			toaleta();

			przerwa();
			badania();

			przerwa();
			poszukiwanieprobek();	

			przerwa();
			sen();
		}
		end_kol--;
	}

	std::string getState() {
		if(state == KolonistaStan::Spi) {
			return "Śpi";
		}
		else if(state == KolonistaStan::Poszukuje_probek) {
			return "Na powierzchni";
		}
		else if(state == KolonistaStan::Kuchnia) {
			return "Posiłek";
		} 
		else if(state == KolonistaStan::Toaleta) {
			return "Toaleta";
		}
		else if(state == KolonistaStan::Bada) {
			return "Bada";
		}
		else if(state == KolonistaStan::Przerwa) {
			return "Przerwa";
		}
		else if(state == KolonistaStan::Ubiera_kombinezon) {
			return "Ubiera kombinezon";
		}
		else if(state == KolonistaStan::Czeka_na_kombinezon) {
			return "Czeka na kombinezon";
		}
		else if(state == KolonistaStan::Zdejmuje_kombinezon) {
			return "Zdejmuję kombinezon";
		}
		else if(state == KolonistaStan::Jedzie_lazikiem) {
			return "Jedzie lazikiem";
		}
		else if(state == KolonistaStan::Wychodzi_na_zew) {
			return "Wychodzi na zew";
		}
		else return "Unknown state";
	}

private:

	SektorMieszkalny& sektorMieszkalny;
	SektorBadan& sektorBadan;
	SektorMagazynowy& sektorMagazynowy;
	Lazik& lazik;

	uint64_t random_num(uint64_t begin, uint64_t end) {
		static thread_local std::random_device rd;
		static thread_local std::mt19937 gen(rd());
		std::uniform_int_distribution<uint64_t> dist(begin,end);
		return dist(gen);
	}

	bool hunger_check() {
		if(hunger <= 40) {
			return true;
		}
		else return false;
	}

	void hunger_roll() {
		hunger-=random_num(10,30);
	}

	void przerwa() {
		state = KolonistaStan::Przerwa;
		hunger_roll();
		if(hunger_check()) {
			posilek();
		}
	}
};
#endif
