#ifndef SEKTORMIESZKALNY_H
#define SEKTORMIESZKALNY_H

#include <vector>
#include <array>
#include <queue>
#include <list>
#include <mutex>
#include <atomic>

#include "Odpad.h"
#include "Owoc.h"
#include "Toaleta.h"
#include "Lozko.h"
#include "Kuchnia.h"
#include "Sluza.h"
#include "Kombinezon.h"
#include "StacjaLadowania.h"

class SektorMieszkalny {
public:

    std::list<Odpad> odpady;
    std::mutex odpady_mtx;
    std::list<Owoc> owoce;
    std::mutex owoce_mtx;
    std::list<int> requestListOwoce;
    std::mutex requestListOwoce_mtx;
    std::vector<Lozko> lozka;
    std::queue<Kombinezon> kombinezony;
    std::array<StacjaLadowania,2> ladowarki;
    std::mutex owoc_waitqueue_mtx;
	std::condition_variable cvar_owoc_waitqueue;

    Toaleta toaleta;
    std::atomic<bool> busy_toilet = false;

    Kuchnia kuchnia;
    std::atomic<bool> busy_kitchen = false;

    std::array<bool, 5> busy_beds = {false, false, false, false, false};
    std::mutex busy_beds_mtx;

    Sluza sluza;
    std::atomic<bool> busy_wew = false;
    std::atomic<bool> busy_zew = false;

    SektorMieszkalny() {
        for(size_t i = 0; i < 5; i++) {
            lozka.emplace_back();
        }
        for(size_t i = 0; i < 3; i++) {
            kombinezony.push(Kombinezon());
        }
    }
};
#endif
