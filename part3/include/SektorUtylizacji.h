#ifndef SEKTORUTYLIZACJI_H
#define SEKTORUTYLIZACJI_H

#include <condition_variable>
#include <mutex>
#include <list>

#include "Odpad.h"
#include "Nawoz.h"
#include "TimeOnMars.h"

class SektorUtylizcji {
public:
    std::condition_variable cv_fert;
    std::mutex fert_mtx;
    std::condition_variable cv_odp;
    std::mutex odp_mtx;
    std::list<Odpad> odpady;
    std::mutex odpady_mtx;
    std::list<Nawoz> nawoz;
    std::mutex nawoz_mtx;
    std::atomic<int>& end_rob;

    SektorUtylizcji(std::atomic<int>& end_rob) :end_rob(end_rob) {

    }

    void operator() () {
        while(end_rob) {
            std::unique_lock<std::mutex> lock(odp_mtx);
            cv_odp.wait_for(lock, std::chrono::milliseconds(10*TimeOnMars::hour), [&]() {
                std::lock_guard<std::mutex> odpady_lock(odpady_mtx);
                return !odpady.empty();
            });
            if(!end_rob){
                break;
            }
			{   
                {
                    std::lock_guard<std::mutex> odpady_lock(odpady_mtx);
                    if(!odpady.empty())
                        odpady.pop_front();
                }
                std::unique_lock<std::mutex> lock_nawz(nawoz_mtx);
                std::this_thread::sleep_for(std::chrono::milliseconds(10 * TimeOnMars::minute));
                nawoz.push_back(Nawoz());
                lock_nawz.unlock();
                std::unique_lock<std::mutex> lock_cv(fert_mtx);
                cv_fert.notify_one();
			}
        }
    }

};
#endif
