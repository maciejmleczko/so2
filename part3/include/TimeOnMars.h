#ifndef TIMEONMARS_H
#define TIMEONMARS_H

#include <cstdint>

class TimeOnMars {
public:
	static constexpr uint16_t day = 24 * 1000; //milisekundy na dzien
	static constexpr uint16_t hour = day / 24;
	static constexpr uint16_t minute = hour / 60;
	static constexpr uint16_t second = minute / 60;
};

#endif
