#ifndef WRITER_H
#define WRITER_H
#include <iostream>
#include <string>
#include <atomic>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <thread>
#include <mutex>
#include <ncurses.h>
#include <math.h>
#include "Filozof.hpp"

class Writer {
	std::vector<Filozof>& zbior_filozofow;
	std::vector<Sztuciec>& stol_z_sztuccami;
	unsigned int lines, columns;
    WINDOW *window = nullptr;
public:
	Writer(std::vector<Filozof>& zbior_filozofow, std::vector<Sztuciec>& stol_z_sztuccami) 
		:zbior_filozofow(zbior_filozofow),
		 stol_z_sztuccami(stol_z_sztuccami)
	{
		setlocale(LC_ALL,"");
		initscr();
	    getmaxyx(stdscr, lines, columns);
		nodelay(stdscr, TRUE);
	    keypad(stdscr, true);
    	cbreak();
   		noecho();
	    start_color();
    	use_default_colors();
    	init_pair(1, COLOR_YELLOW, COLOR_BLACK);
		init_pair(2, COLOR_GREEN, COLOR_BLACK);

		window = newwin(lines - 10, columns - 15, 1, 4);
    	keypad(window, true);

    	box(window, 0, 0);
		wattron(window,A_BOLD);
    	mvwprintw(window, 1, 1, "%s", "Id");
    	mvwprintw(window, 1, 5, "%s", "Filozofowie");
    	mvwprintw(window, 1, (columns/2) - 10, "%s", "Id");
    	mvwprintw(window, 1, (columns/2) - 6, "%s", "Widelce");
    	mvwprintw(window, 1, (columns/2) + 6, "%s", "Właściciel");
		wattroff(window,A_BOLD);
   		refresh();

    	wrefresh(window);

    	refresh();
	}
	~Writer() {
		endwin();
	}
	void start(std::atomic<bool>& end) {
		int op;
		unsigned int strona = 1;
		unsigned int pos_per_page;
		unsigned int page_limit = 1;

		if(zbior_filozofow.size() <= lines - 14) {
			pos_per_page = zbior_filozofow.size();
		}
		else {
			pos_per_page = lines - 14;
			page_limit = zbior_filozofow.size()/(lines - 14) + 1;
		}

		while((op = getch()) and !end) {
			if(op == 110) {
				if( strona < page_limit ) {
					strona++;
				}
			}
			else if(op == 112) {
				if( strona > 1 ) {
					strona--;
				}
			}
			size_t max_przedzial;
			if(pos_per_page * strona > zbior_filozofow.size())
				max_przedzial = zbior_filozofow.size();
			else max_przedzial = pos_per_page * strona;
			
			for(size_t j = 0; j < lines - 14; j++) {
    			mvwprintw(window, 2 + j, 1, "%s", "  ");
    			mvwprintw(window, 2 + j, 5, "%s", "          ");
    			mvwprintw(window, 2 + j, 18, "%s", "         ");
    			mvwprintw(window, 2 + j, (columns/2) - 10, "%s", "   ");
    			mvwprintw(window, 2 + j, (columns/2) - 6, "%s", "         ");
    			mvwprintw(window, 2 + j, (columns/2) + 10, "%s", "         ");
			}

			for(size_t i = 0 + pos_per_page * (strona - 1), j = 0; i < max_przedzial; i++, j++) {
    			mvwprintw(window, 2 + j, 1, "%s", zbior_filozofow[i].getId().c_str());
    			mvwprintw(window, 2 + j, 5, "%s", zbior_filozofow[i].getState().c_str());
    			mvwprintw(window, 2 + j, 18, "%d", zbior_filozofow[i].state_time);

    			mvwprintw(window, 2 + j, (columns/2) - 10, "%s", zbior_filozofow[i].getId().c_str());
    			mvwprintw(window, 2 + j, (columns/2) - 6, "%s", "         ");
    			mvwprintw(window, 2 + j, (columns/2) - 6, "%s", stol_z_sztuccami[i].getState().c_str());
    			mvwprintw(window, 2 + j, (columns/2) + 10, "%d", stol_z_sztuccami[i].current_owner);
			}
    		wrefresh(window);
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			if(op == 113) {
				end = true;
			}
		}
	}
};

#endif
