#ifndef FILOZOF_HPP
#define FILOZOF_HPP
#include <random>
#include <array>
#include <vector>
#include <atomic>
#include <memory>
#include <thread>
#include <mutex>
#include <chrono>
#include <iostream>
#include <sstream>
#include "Sztuciec.hpp"

using std::endl;

enum struct FilState {
	Poczatek, Posilek, Czeka_na_lewy, Czeka_na_prawy, Odpoczynek, Koniec
};

class Filozof {
	std::atomic<bool>& stop_flag;
	Sztuciec& widelec_l;
	Sztuciec& widelec_p;

	void podnies_widelce();
	bool posilek();
	void odloz_widelce();
	bool filozofowanie();

public:
	unsigned int id;
	FilState state;
	uint64_t state_time;

	Filozof(unsigned int, std::atomic<bool>&, Sztuciec&, Sztuciec&);
	void operator() ();

	std::string getId();
	std::string getState();
};

#endif
