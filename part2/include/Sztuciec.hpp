#ifndef SZTUCIEC_H
#define SZTUCIEC_H
#include <atomic>
#include <mutex>
#include <condition_variable>

enum struct ForkState {
	Czysty, Brudny
};

class Sztuciec {
public:
	std::mutex sync_mtx;
	std::condition_variable cond_var;
	std::mutex frk_mtx;
	ForkState state = ForkState::Brudny;
	int current_owner = -1;

	Sztuciec() = default;
	Sztuciec(const Sztuciec&);
	void give(int);
	void rel();
	std::string getState();
};
#endif
