#include <Filozof.hpp>

bool end_day = true;
bool continue_day = false;

void Filozof::podnies_widelce() {
	state = FilState::Czeka_na_lewy;
	widelec_l.give(id);
	state = FilState::Czeka_na_prawy;
	widelec_p.give(id);
}

bool Filozof::posilek() {
	static thread_local std::random_device rd;
	static thread_local std::mt19937 gen(rd());

	std::lock_guard<std::mutex> lfork(widelec_l.frk_mtx);
	std::lock_guard<std::mutex> rfork(widelec_p.frk_mtx);
	state = FilState::Posilek;

	std::uniform_int_distribution<uint64_t> dist(0,10);
	int state_time = 2000 + 100*dist(gen);

	while(state_time > 0 ) {
		if(stop_flag) {
			odloz_widelce();
			return end_day;
		}
		state_time-=100;
		this->state_time=state_time;
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}

	return continue_day;
}

void Filozof::odloz_widelce() {
	widelec_l.rel();
	widelec_p.rel();
}

bool Filozof::filozofowanie() {
	static thread_local std::random_device rd;
	static thread_local std::mt19937 gen(rd());

	std::uniform_int_distribution<uint64_t> dist(0,10);
	int state_time = 2000 + 100*dist(gen);

	while(state_time > 0 ) {
		if(stop_flag) {
			return end_day;
		}
		state_time-=100;
		this->state_time=state_time;
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
	return continue_day;
}


Filozof::Filozof(unsigned int id, std::atomic<bool>& stop_flag, Sztuciec& widelec_l, Sztuciec& widelec_p) 
	:id(id),
	stop_flag(stop_flag),
	widelec_l(widelec_l),
	widelec_p(widelec_p) {}

void Filozof::operator() () {
	while(true) {
		podnies_widelce();
		
		if(posilek() == end_day)
			break;

		odloz_widelce();

		state = FilState::Odpoczynek;
		if(filozofowanie() == end_day)
			break;
	}
	state = FilState::Koniec;
}

std::string Filozof::getId() {
	std::stringstream ss;
	ss<<id;
	std::string id_string(ss.str());
	id_string.append("\0");
	return id_string;
}

std::string Filozof::getState() {
	std::string state_string;
	if(state == FilState::Posilek)
		state_string = "Posilek   ";
	else if(state == FilState::Odpoczynek) 
		state_string = "Odpoczynek";
	else if(state == FilState::Poczatek)
		state_string = "Poczatek  ";
	else if(state == FilState::Czeka_na_lewy)
		state_string = "Czeka na L";
	else if(state == FilState::Czeka_na_prawy)
		state_string = "Czeka na P";
	else
		state_string = "Koniec    ";
	state_string.append("\0");
	return state_string;
}
