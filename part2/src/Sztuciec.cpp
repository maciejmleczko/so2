#include "Sztuciec.hpp"


Sztuciec::Sztuciec(const Sztuciec& sztuciec) 
	:state(sztuciec.state),
	current_owner(sztuciec.current_owner) {}

void Sztuciec::give(int id_filozofa) {
	while (id_filozofa != current_owner) {
        if (state == ForkState::Brudny) {
            std::lock_guard<std::mutex> lock(frk_mtx);
            current_owner = id_filozofa;
			state = ForkState::Czysty;
        } 
		else {
			std::unique_lock<std::mutex> ulock(sync_mtx);
			cond_var.wait(ulock);
        }
	}
}

void Sztuciec::rel() {
	state = ForkState::Brudny;
	std::unique_lock<std::mutex> ulock(sync_mtx);
	cond_var.notify_all();
}

std::string Sztuciec::getState() {
	std::string state_string;
	if(state == ForkState::Brudny)
		state_string = "Brudny";
	else if(state == ForkState::Czysty) 
		state_string = "Czysty";
	state_string.append("\0");
	return state_string;
}
