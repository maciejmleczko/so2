#include <thread>
#include <atomic>
#include <string>
#include <iostream>
#include <chrono>
#include "Filozof.hpp"
#include "Writer.hpp"

int analize_args(int argc, char* argv[]) {
	const int defltval = 5;	
	bool expectval = false;
	std::vector<std::string> args(argv, argv+argc);
	for(auto& arg : args) {
		if(expectval) {
			int retval = std::stoi(arg);
			if(retval >= 5)
				return retval;
			else return defltval;
		}
		else {
			if(arg == "-n")
				expectval = true;
		}
	}
	return defltval;
}

int main(int argc, char* argv[]) {

	size_t liczba_filozofow = analize_args(argc, argv);

	std::vector<Sztuciec> stol_z_sztuccami;
	stol_z_sztuccami.reserve(liczba_filozofow);

	for(size_t i = 0; i < liczba_filozofow; i++)
		stol_z_sztuccami.emplace_back();

	std::vector<Filozof> zbior_filozofow;
	zbior_filozofow.reserve(liczba_filozofow);

	std::atomic<bool> end(false);

	for(size_t i = 0; i < liczba_filozofow; i++) {
		if(i == 0) {
			zbior_filozofow.emplace_back(i, end, stol_z_sztuccami[liczba_filozofow-1], stol_z_sztuccami[0]);
		}
		else zbior_filozofow.emplace_back(i, end, stol_z_sztuccami[i - 1], stol_z_sztuccami[i]);
	}

	std::vector<std::thread> watki_filozofow;

	auto ui_thread_fun = [&zbior_filozofow, &stol_z_sztuccami, &end]() {
		Writer ui{zbior_filozofow, stol_z_sztuccami};
		ui.start(end);
	};

	std::thread ui_thread(ui_thread_fun);
	ui_thread.detach();

	for(size_t i = 0; i < liczba_filozofow; i++) {
		watki_filozofow.push_back(std::thread(std::ref(zbior_filozofow[i])));
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}

	for(size_t i = 0; i < liczba_filozofow; i++)
		watki_filozofow[i].join();

	return 0;
}
