#include <thread>
#include <atomic>
#include <string>
#include <iostream>
#include <chrono>
#include "Filozof.hpp"

int analize_args(int argc, char* argv[]) {
	const int defltval = 5;	
	bool expectval = false;
	std::vector<std::string> args(argv, argv+argc);
	for(auto& arg : args) {
		if(expectval) {
			int retval = std::stoi(arg);
			if(retval >= 5)
				return retval;
			else return defltval;
		}
		else {
			if(arg == "-n")
				expectval = true;
		}
	}
	return defltval;
}

int main(int argc, char* argv[]) {

	size_t liczba_filozofow = analize_args(argc, argv);

	std::vector<Sztuciec> stol_z_sztuccami;
	stol_z_sztuccami.reserve(liczba_filozofow);

	for(size_t i = 0; i < 2*liczba_filozofow; i++)
		stol_z_sztuccami.emplace_back();

	std::vector<Filozof> zbior_filozofow;
	zbior_filozofow.reserve(liczba_filozofow);

	std::atomic<bool> end(false);

	for(size_t i = 0; i < liczba_filozofow; i++) 
		zbior_filozofow.emplace_back(i, stol_z_sztuccami, end);

	std::vector<std::thread> watki_filozofow;

	for(size_t i = 0; i < liczba_filozofow; i++) {
		watki_filozofow.push_back(std::thread(std::ref(zbior_filozofow[i])));
		std::this_thread::sleep_for(std::chrono::milliseconds(1500));
	}

	while(true) {
		char key;
		key = std::cin.get();	
		if(key==27) {
			end = true;
			break;
		}
	}

	for(size_t i = 0; i < liczba_filozofow; i++)
		watki_filozofow[i].join();

	return 0;
}
