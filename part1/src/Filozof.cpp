#include <Filozof.hpp>

std::mutex Filozof::outstream_mutex;
bool end_day = true;
bool continue_day = false;

void Filozof::podnies_widelec(unsigned int numer) {
	stol_z_sztuccami[numer].current_owner = id;

	std::stringstream message;
	message<<"Filozof "<<id<<"\033[1;37m podnosi widelec \033[0m"<<numer<<endl;
	Writer::writeMessage(message.str(), outstream_mutex);
}

bool Filozof::posilek() {
	static thread_local std::random_device rd;
	static thread_local std::mt19937 gen(rd());

	std::uniform_int_distribution<uint64_t> dist(0,10);
	int state_time = 2000 + 100*dist(gen);
	
	std::stringstream message;
	message<<"Filozof "<<id<<"\033[1;33m spożywa posiłek\033[0m"<<endl;
	Writer::writeMessage(message.str(), outstream_mutex);

	while(state_time >= 0 ) {
		if(stop_flag) {
			return end_day;
		}
		state_time-=100;
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
	return continue_day;
}

void Filozof::odloz_widelec(unsigned int numer) {
	stol_z_sztuccami[numer].current_owner = -1;

	std::stringstream message;
	message<<"Filozof "<<id<<"\033[1;37m odkłada widelec \033[0m"<<numer<<endl;
	Writer::writeMessage(message.str(), outstream_mutex);
}

bool Filozof::filozofowanie() {
	static thread_local std::random_device rd;
	static thread_local std::mt19937 gen(rd());

	std::uniform_int_distribution<uint64_t> dist(0,10);
	int state_time = 2000 + 100*dist(gen);

	std::stringstream message;
	message<<"Filozof "<<id<<"\033[1;36m filozofuje \033[0m"<<endl;
	Writer::writeMessage(message.str(), outstream_mutex);

	while(state_time >= 0 ) {
		if(stop_flag) {
			return end_day;
		}
		state_time-=100;
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
	return continue_day;
}


Filozof::Filozof(unsigned int id, std::vector<Sztuciec>& stol_z_sztuccami, std::atomic<bool>& stop_flag) 
	:id(id),
	stol_z_sztuccami(stol_z_sztuccami),
	stop_flag(stop_flag) {}

/*
Filozof::Filozof(Filozof&& filozof) 
	:id(std::move(filozof.id)), 
	stol_z_sztuccami(std::move(filozof.stol_z_sztuccami)) {}
*/

void Filozof::operator() () {
	std::stringstream message;
	message<<"Filozof "<<id<<"\033[1;32m zaczyna dzień! \033[0m"<<endl;
	Writer::writeMessage(message.str(), outstream_mutex);
	message.str(std::string());

	while(true) {
		podnies_widelec(2*id);
		podnies_widelec(2*id + 1);

		if(posilek() == end_day)
			break;

		odloz_widelec(2*id + 1);
		odloz_widelec(2*id);

		if(filozofowanie() == end_day)
			break;
	}

	message<<"Filozof "<<id<<" \033[1;31mkończy dzień!\033[0m"<<endl;
	Writer::writeMessage(message.str(), outstream_mutex);
}
