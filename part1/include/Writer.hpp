#ifndef WRITER_H
#define WRITER_H
#include <iostream>
#include <string>
#include <mutex>

class Writer {
	public:
	static void writeMessage(std::string message, std::mutex& mtx) {
		std::lock_guard<std::mutex> guard(mtx);
		std::cout<<message;
	}
};

#endif
