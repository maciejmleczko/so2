#ifndef FILOZOF_HPP
#define FILOZOF_HPP
#include <random>
#include <array>
#include <vector>
#include <atomic>
#include <memory>
#include <thread>
#include <mutex>
#include <chrono>
#include <iostream>
#include <sstream>
#include "Sztuciec.hpp"
#include "Writer.hpp"

using std::endl;

class Filozof {
	std::vector<Sztuciec> stol_z_sztuccami;
	static std::mutex outstream_mutex;
	std::atomic<bool>& stop_flag;

	void podnies_widelec(unsigned int);
	bool posilek();
	void odloz_widelec(unsigned int);
	bool filozofowanie();

public:
	unsigned int id;
	std::string state;
	uint64_t state_time;

	Filozof(unsigned int, std::vector<Sztuciec>&, std::atomic<bool>&);
	void operator() ();
};

#endif
